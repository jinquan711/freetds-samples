# freetds-samples

#### Description
freetds:  mssql(Microsoft SQL Server) C语言Linux客户端连接库

freetds-samples: freetds在CentOS7.5下的简要例程


### 测试流程
1、yum install -y freetds freetds-devel

$ sudo yum install -y glib2-devel

```
Linux中的程序访问Microsoft SQL Server，使用freetds库
/usr/lib64/libsybdb.so.5.1.0
/usr/include/sybdb.h
/usr/include/syberror.h
/usr/include/sybfront.h
```

2、gcc sample*.c -L/usr/lib64 -lsybdb -o sample*

3、./sample*