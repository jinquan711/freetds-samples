#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include "mssql_common.h"

int main(int argc, char* argv[])
{
	if(argc == 1)
		return 1;
	else if(argc > 1)
	{
		struct __database_link dblink;
		MSSQL_CONN conn;

	    DATABASE_LINK_CONSTRUCT(&dblink, "172.24.1.122", 1433,
                        				  "sa", "4aKadedr",
                        				  "testDB");  
	 	MSSQL_CONN_OPEN(&conn, &dblink);

		mssql_query_column_name(&conn, argv[1]);
		mssql_rstset_print(&(conn.result));

		for(int i = 0; i < conn.result.nCOL; i++)
		{
			printf("%d\t", conn.result.SIZE[i]);
		}printf("\n");

		for(int i = 0; i < conn.result.nCOL; i++)
		{
			printf("%d\t", conn.result.TYPE[i]);
		}printf("\n");
		

		MSSQL_CONN_RSTSET_CLEAN(&(conn.result));

	   	dbclose(conn.dbprocess);
		dbexit();
		
		return 0;
	}

}