#ifndef __tds_select_h__
#define __tds_select_h__

#include "tds_conn.h"

/*
 * SELECT 查询语句的处理函数返回值
 */
#define  SELECT_DBRESULT_FAIL	 			    -1
#define	 SELECT_ALLOC_ALLCOL_FIELDS_FAIL	 	-2
#define  SELECT_ALLOC_RSTSET_FAIL				-3
#define	 SELECT_ALLOC_FIELDS_SHRBUF_FAIL 		-4
#define  SELECT_BIND_FIELDS_SHRBUF_FAIL			-5
#define	 SELECT_NULLBIND_FIELDS_STATUS_FAIL		-6
#define  SELECT_ALLOC_COLNAME_FAIL				-7
#define  SELECT_DBNEXTROW_FAIL					-8
#define  SELECT_DBNEXTROW_REALLOC_RSTSET_FAIL	-9
#define  SELECT_DBNEXTROW_ALLOC_FIELD_FAIL		-10

extern void mssql_select_clean(MSSQL_CONN* conn);
extern void mssql_select_print(MSSQL_CONN* conn);
extern int mssql_select(MSSQL_CONN* conn);

#endif
