#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include "tds_conn.h"
#include "tds_select.h"

extern void mssql_create_table(MSSQL_CONN* conn, char* table_name);
extern void mssql_insert_table(struct __access_log_data_line_type* line,
                    MSSQL_CONN* conn, char* table);

int main(int argc, char* argv[])
{
    MSSQL_CONN conn;
    mssql_conn_construct(&conn, "172.24.1.122", 1433,
                            "sa", "4aKadedr",
                            "testDB");
    //建立到MSSQL数据库的连接------------------------   
    mssql_conn_open(&conn);
    //开始进行数据库中数据查询的工作------------------  
    char mssqlbuf[1024]; memset(mssqlbuf, 0x00, sizeof(mssqlbuf));               
    sprintf(mssqlbuf, "SELECT name, age FROM testTable");   
    dbcmd(conn.dbprocess, mssqlbuf);                             //将刚刚组装好的sql命令, 使用dbcmd命令保存到数据库连接句柄的缓存中  
                              
    if(dbsqlexec(conn.dbprocess) == FAIL)                        //如果执行的命令失败  
    {   
        dbclose(conn.dbprocess);                                 //关闭数据库操作进程  
        exit(EXIT_FAILURE);                            
    }  
    
    int nDatOpt = mssql_select(&conn);
    mssql_select_print(&conn);
    mssql_select_clean(&conn);

    dbclose(conn.dbprocess);                                     //关闭数据库连接   
    dbexit();

    return 0;
}