#ifndef __tds_conn_h__
#define __tds_conn_h__

#include <sybfront.h>  
#include <sybdb.h> 

typedef struct __mssql_conn
{
    char SQL_SERVER_IP[16];
    char SQL_SERVER_PORT[8];
    char SQL_SERVER[32];
    char user[32];
    char pass[64];
    char db[16];
    char table[32];

    DBINT result_code;  //定义保存数据库交易结果的类型
    DBPROCESS *dbprocess;   //数据库连接结果的句柄 
    //字符串数组格式的结果集，所有数据类型均以字符串格式保存
    char **RSTSET;
    int RSTnCOL;
    int RSTnROW;
}MSSQL_CONN;

extern void mssql_conn_construct(MSSQL_CONN* conn, char* SQL_SERVER_IP, int SQL_SERVER_PORT,
                            char* user, char* pass,
                            char* db );

extern int mssql_conn_open(MSSQL_CONN* conn);

#endif
