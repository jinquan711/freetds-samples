#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include "tds_conn.h"

void mssql_conn_construct(MSSQL_CONN* conn, char* SQL_SERVER_IP, int SQL_SERVER_PORT,
                            char* user, char* pass,
                            char* db )
{
    //char s_port[25];
    //itoa(SQL_SERVER_PORT, s_port, 10);     //here 10 means decimal
    strcpy(conn->SQL_SERVER_IP, SQL_SERVER_IP);
   sprintf(conn->SQL_SERVER_PORT, "%d", SQL_SERVER_PORT);
   sprintf(conn->SQL_SERVER, "%s:%d", SQL_SERVER_IP, SQL_SERVER_PORT);
    strcpy(conn->user, user);
    strcpy(conn->pass, pass);
    strcpy(conn->db, db);
    //strcpy(conn->table, table);
}

int mssql_conn_open(MSSQL_CONN* conn)
{
    //建立到MSSQL数据库的连接------------------------   
    dbinit();                         //使用dbinit()函数, 进行freetds操作之前的初始化操作  
    LOGINREC *loginrec = dblogin();   //建立一个到数据库的连接的句柄  
    DBSETLUSER(loginrec, conn->user);  //向句柄中添加连接数据库的用户  
    DBSETLPWD(loginrec,  conn->pass);                     //向数据库连接句柄中添加密码  
    conn->dbprocess = dbopen(loginrec, conn->SQL_SERVER);   //连接数据库,并返回数据库连接结果的句柄  
          
    if(conn->dbprocess == FAIL)                                   //如果连接失败  
    {   
        fprintf(stderr, "Connect Fail\n");                  //在标准错误中输出信息  
        //exit(EXIT_FAILURE);                                 //进程异常退出  
        return 1;
    }  
    else                                                    //如果连接成功  
    {  
        printf("Connect success\n");  
    }  
           
    if(dbuse(conn->dbprocess, conn->db) == FAIL)                 //使用某个数据库，如果使用失败  
    {  
        dbclose(conn->dbprocess);                                 //关闭数据库连接句柄, 并且回收相关资源  
        //exit(EXIT_FAILURE);                                 //进程异常退出  
        return 2;
    }  
    return 0;
}