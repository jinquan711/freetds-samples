#ifndef __mssql_common_h__
#define __mssql_common_h__

/***************************************************************
 * 链接freetds-devel库： /usr/lib64/libsybdb.so.5.1.0
 * $ gcc sample3.c -L/usr/lib64 -lsybdb -o sample3
 * Windows Server 防火墙需要创建mssql入站规则：打开默认的1433端口
 ***************************************************************/
#include <sybfront.h>  
#include <sybdb.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 

#if 0
typedef enum {
    SIZE_bigint      = 8,
    SIZE_binary50    = 50,
    SIZE_bit         = 1,
    SIZE_char10      = 10,
    SIZE_date        = 20,
    SIZE_datetime    = 8,
    SIZE_datetime27  = 54,
    SIZE_datetimeoffset7 = 68,
    SIZE_decimal18   = 8,
    SIZE_float       = 8,
    SIZE_geography   = 64512,
    SIZE_geometry    = 64512,
    SIZE_hierarchyid = 255,
    SIZE_image       = 64512,
//    
    SIZE_int         = 4,
    SIZE_money       = 8,
    SIZE_nchar10     = 20,
    SIZE_numeric18   = 8,
    SIZE_nvarchar50  = 100,
    SIZE_real        = 4,
    SIZE_smalldatetime = 4,
    SIZE_smallint    = 2,
    SIZE_smallmoney  = 4,
    SIZE_sql_variant = 255,
    SIZE_text        = 64512,
    SIZE_time7       = 32,
    SIZE_timestamp   = 8,
    SIZE_tinyint     = 1,
    SIZE_uniqueidentifier = 16,
    SIZE_varbinary50 = 50,
    SIZE_varbinarymax = 64512,
    SIZE_varchar50   = 50
}MSSQL2014_COL_SIZE;

typedef enum {
    TYPE_bigint      = 62,
    TYPE_binary50    = 45,
    TYPE_bit         = 50,
    TYPE_char10      = 47,
    TYPE_date        = 47,
    TYPE_datetime    = 61,
    TYPE_datetime27  = 47,
    TYPE_datetimeoffset7 = 47,
    TYPE_decimal18   = 62,
    TYPE_float       = 62,
    TYPE_geography   = 34,
    TYPE_geometry    = 34,
    TYPE_hierarchyid = 45,
    TYPE_image       = 34,
//    
    TYPE_int         = 56,
    TYPE_money       = 60,
    TYPE_nchar10     = 47,
    TYPE_numeric18   = 62,
    TYPE_nvarchar50  = 47,
    TYPE_real        = 59,
    TYPE_smalldatetime = 58,
    TYPE_smallint    = 52,
    TYPE_smallmoney  = 122,
    TYPE_sql_variant = 47,
    TYPE_text        = 35,
    TYPE_time7       = 47,
    TYPE_timestamp   = 45,
    TYPE_tinyint     = 48,
    TYPE_uniqueidentifier = 45,
    TYPE_varbinary50 = 45,
    TYPE_varbinarymax = 34,
    TYPE_varchar50   = 47
}MSSQL2014_COL_TYPE;
#endif

typedef enum{
    MSSQL2014_AVAIL_COL_SIZE_bit         = 1,
    MSSQL2014_AVAIL_COL_SIZE_char10      = 10,
    MSSQL2014_AVAIL_COL_SIZE_datetime    = 8,
    MSSQL2014_AVAIL_COL_SIZE_datetime27  = 54,
    MSSQL2014_AVAIL_COL_SIZE_datetimeoffset7 = 68,
    MSSQL2014_AVAIL_COL_SIZE_hierarchyid = 255,
    MSSQL2014_AVAIL_COL_SIZE_int         = 4,
    MSSQL2014_AVAIL_COL_SIZE_money       = 8,
    MSSQL2014_AVAIL_COL_SIZE_nvarchar50  = 100,
    MSSQL2014_AVAIL_COL_SIZE_real        = 4,
    MSSQL2014_AVAIL_COL_SIZE_smalldatetime = 4,
    MSSQL2014_AVAIL_COL_SIZE_smallint    = 2,
    MSSQL2014_AVAIL_COL_SIZE_smallmoney  = 4,
    MSSQL2014_AVAIL_COL_SIZE_sql_variant = 255,
    MSSQL2014_AVAIL_COL_SIZE_text        = 64512,
    MSSQL2014_AVAIL_COL_SIZE_time7       = 32,
    MSSQL2014_AVAIL_COL_SIZE_timestamp   = 8,
    MSSQL2014_AVAIL_COL_SIZE_tinyint     = 1,
    MSSQL2014_AVAIL_COL_SIZE_uniqueidentifier = 16,
    MSSQL2014_AVAIL_COL_SIZE_varchar50   = 50
}MSSQL2014_AVAIL_COL_SIZE;

typedef enum{
    MSSQL2014_AVAIL_COL_TYPE_bit         = 50,
    MSSQL2014_AVAIL_COL_TYPE_char10      = 47,
    MSSQL2014_AVAIL_COL_TYPE_datetime    = 61,
    MSSQL2014_AVAIL_COL_TYPE_datetime27  = 47,
    MSSQL2014_AVAIL_COL_TYPE_datetimeoffset7 = 47,
    MSSQL2014_AVAIL_COL_TYPE_hierarchyid = 45,
    MSSQL2014_AVAIL_COL_TYPE_int         = 56,
    MSSQL2014_AVAIL_COL_TYPE_money       = 60,
    MSSQL2014_AVAIL_COL_TYPE_nvarchar50  = 47,
    MSSQL2014_AVAIL_COL_TYPE_real        = 59,
    MSSQL2014_AVAIL_COL_TYPE_smalldatetime = 58,
    MSSQL2014_AVAIL_COL_TYPE_smallint    = 52,
    MSSQL2014_AVAIL_COL_TYPE_smallmoney  = 122,
    MSSQL2014_AVAIL_COL_TYPE_sql_variant = 47,
    MSSQL2014_AVAIL_COL_TYPE_text        = 35,
    MSSQL2014_AVAIL_COL_TYPE_time7       = 47,
    MSSQL2014_AVAIL_COL_TYPE_timestamp   = 45,
    MSSQL2014_AVAIL_COL_TYPE_tinyint     = 48,
    MSSQL2014_AVAIL_COL_TYPE_uniqueidentifier = 45,
    MSSQL2014_AVAIL_COL_TYPE_varchar50   = 47
}MSSQL2014_AVAIL_COL_TYPE;


#define    MSSQL2014_AVAIL_COL_C_TYPE_bit              "%%d"
#define    MSSQL2014_AVAIL_COL_C_TYPE_char10           "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_datetime         "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_datetime27       "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_datetimeoffset7  "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_hierarchyid      "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_int              "%%ld"
#define    MSSQL2014_AVAIL_COL_C_TYPE_money            "%%f"
#define    MSSQL2014_AVAIL_COL_C_TYPE_nvarchar50       "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_real             "%%f"
#define    MSSQL2014_AVAIL_COL_C_TYPE_smalldatetime    "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_smallint         "%%d"
#define    MSSQL2014_AVAIL_COL_C_TYPE_smallmoney       "%%f"
#define    MSSQL2014_AVAIL_COL_C_TYPE_sql_variant      "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_text             "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_time7            "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_timestamp        "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_tinyint          "%%d"
#define    MSSQL2014_AVAIL_COL_C_TYPE_uniqueidentifier "%%s"
#define    MSSQL2014_AVAIL_COL_C_TYPE_varchar50        "%%s"


typedef struct __result_set{
    int *SIZE;
    int *TYPE;
    //字符串数组格式的结果集，所有数据类型均以字符串格式保存
    char **SET;
    int nCOL;
    int nROW;    
}RSTSET;

typedef struct __database_link{
    char SQL_SERVER_IP[16];
    char SQL_SERVER_PORT[8];
    char SQL_SERVER[32];
    char user[32];
    char pass[64];
    char db[16];
}DB_LINK;

typedef struct __mssql_conn
{
    DB_LINK *dblink;   //数据库连接参数
    char table[32];
    DBINT result_code;  //定义保存数据库交易结果的类型
    DBPROCESS *dbprocess;   //数据库连接结果的句柄 
    RSTSET result;  //查询结果集
}MSSQL_CONN;

void DATABASE_LINK_CONSTRUCT(struct __database_link* dblink, char* SQL_SERVER_IP, int SQL_SERVER_PORT,
                            char* user, char* pass,
                            char* db );
int MSSQL_CONN_OPEN(MSSQL_CONN* conn, struct __database_link* dblink);
void MSSQL_CONN_RSTSET_CLEAN(RSTSET* rstset);
void mssql_rstset_print(RSTSET* rstset);
int mssql_select(MSSQL_CONN* conn);
int mssql_query_column_name(MSSQL_CONN *conn, char* table);

#endif